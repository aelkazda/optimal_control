import numpy as np
import matplotlib.pyplot as plt


def plot_continuous(
    trajectory, plot_func, t_i, t_f, n_samples, fig=None, ax=None, log=False
):
    """
    Plot plot_func(solution) from t_i to t_f
    """
    if fig == None or ax == None:
        fig, ax = plt.subplots(figsize=(10, 10))
    t = np.linspace(t_i, t_f, n_samples, endpoint=True)
    first = plot_func(trajectory(t[0]))
    y = np.empty((t.size, len(first)))
    y[0] = first
    for i in range(1, t.size):
        y[i, :] = plot_func(trajectory(t[i]))

    for i in range(y.shape[1]):
        if log:
            ax.semilogy(t, y[:, i])
        else:
            ax.plot(t, y[:, i])
    return fig, ax


def plot_discrete(
    trajectory,
    plot_func,
    begin=0,
    end=None,
    plot_ti=0.0,
    plot_dt=1.0,
    fig=None,
    ax=None,
    log=False,
):
    if fig == None or ax == None:
        fig, ax = plt.subplots(figsize=(10, 10))

    end = len(trajectory) if (end is None) else end
    n_samples = end - begin
    plot_tf = plot_ti + n_samples * plot_dt

    t = plot_ti + ((plot_tf - plot_ti) / n_samples) * np.arange(n_samples)
    first = plot_func(trajectory[begin])
    y = np.empty((n_samples, len(first)))

    y[0] = first
    for i in range(1, n_samples):
        y[i, :] = plot_func(trajectory[begin + i])

    for i in range(y.shape[1]):
        if log:
            ax.semilogy(t, y[:, i])
        else:
            ax.plot(t, y[:, i], linestyle="dashed")

    return fig, ax
