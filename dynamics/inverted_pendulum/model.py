import math
import numpy as np
from ...base import model
from .const import m_cart, m_ball, l, g


def f(x=None, u=None):
    """
    x: State of the double pendulum
    [cart_position, angle, cart_velocity, angular_velocity]
    u: Control

    Returns:
        Derivative of the state with respect to time
    """
    c, s = math.cos(x[1]), math.sin(x[1])
    accel = (u[0] - m_ball * l * x[3] * x[3] * s + m_ball * g * c * s) / (
        m_cart + m_ball * (1.0 - c * c)
    )
    angular_accel = (accel * c + g * s) / l
    return np.array([x[2], x[3], accel, angular_accel])


func = model.SystemFunc(f)
system = model.ContinuousSystem(4, func, 0.0, 20.0)
