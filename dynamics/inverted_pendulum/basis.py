import math
import numpy as np


def polynomial(Q=0, vectorized=False, use_sympy=False):
    out = []

    for a in range(Q + 1):
        b_upper = Q - a + 1
        for b in range(b_upper):
            c_upper = Q - a - b + 1
            for c in range(c_upper):
                d_upper = Q - a - b - c + 1
                for d in range(d_upper):
                    e_upper = Q - a - b - c - d + 1
                    for e in range(e_upper):
                        if vectorized:
                            out.append(
                                lambda x, u, a=a, b=b, c=c, d=d, e=e: (
                                    (x[:, 0]) ** a
                                    * (x[:, 1]) ** b
                                    * (x[:, 2]) ** c
                                    * (x[:, 3]) ** d
                                    * (u[:, 0]) ** d
                                )
                            )
                        else:
                            out.append(
                                lambda x, u, a=a, b=b, c=c, d=d, e=e: (
                                    (x[0]) ** a
                                    * (x[1]) ** b
                                    * (x[2]) ** c
                                    * (x[3]) ** d
                                    * u[0] ** e
                                )
                            )

    return out


def fourier_poly(Q=0, vectorized=False, use_sympy=False):
    out = []

    if vectorized:
        out.append(lambda x, u: x[:, 1])
        sin = np.sin
        cos = np.cos
    else:
        out.append(lambda x, u: x[1])
        sin = math.sin
        cos = math.cos

    if use_sympy:
        sin = sympy.sin
        cos = sympy.cos

    for a in range(Q + 1):
        b_upper = Q + 1
        for b in range(b_upper):
            c_upper = Q + 1 - b
            for c in range(c_upper):
                d_upper = Q + 1 - b - c
                for d in range(d_upper):
                    e_upper = Q + 1 - b - c - d
                    for e in range(e_upper):
                        for f in (cos, sin):
                            if a != 0 or f != sin:
                                if vectorized:
                                    out.append(
                                        lambda x, u, f=f, a=a, b=b, c=c, d=d, e=e: (
                                            f(a * x[:, 1])
                                            * (x[:, 0]) ** b
                                            * (x[:, 2]) ** c
                                            * (x[:, 3]) ** d
                                            * u[:, 0] ** e
                                        )
                                    )
                                else:
                                    out.append(
                                        lambda x, u, f=f, a=a, b=b, c=c, d=d, e=e: (
                                            f(a * x[1])
                                            * (x[0]) ** b
                                            * (x[2]) ** c
                                            * (x[3]) ** d
                                            * u[0] ** e
                                        )
                                    )
    return out
