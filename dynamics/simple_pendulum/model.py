import math
import numpy as np
from .const import l, g
from ...base import model


def f(t=None, x=None, u=None):
    """
    t: Time (None)
    x: State of the simple pendulum
    [angle, angular_velocity]
    u: Control (None)

    Returns:
        Derivative of the state with respect to time
    """
    return np.array((x[1], -g / l * math.sin(x[0])))


func = model.SystemFunc(f)
system = model.ContinuousSystem(2, func, 0.0, 20.0)
