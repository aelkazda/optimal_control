import numpy as np
from .const import l_theta, l_theta_dot


def polynomial(Q=0, vectorized=False, use_sympy=False):
    out = []
    for i in range(2):
        if vectorized:
            out.append(lambda x, i=i: x[:, i])
        else:
            out.append(lambda x, i=i: x[i])

    for a in range(Q + 1):
        b_upper = Q - a + 1
        for b in range(b_upper):
            if vectorized:
                out.append(
                    lambda x, a=a, b=b: (
                        (x[:, 0] / l_theta) ** a * (x[:, 1] / l_theta_dot) ** b
                    )
                )
            else:
                out.append(
                    lambda x, a=a, b=b: (
                        (x[0] / l_theta) ** a * (x[1] / l_theta_dot) ** b
                    )
                )

    return out


def fourier_poly(Q=0, vectorized=False, use_sympy=False):
    out = []
    if vectorized:
        out.append(lambda x: x[:, 0])
    else:
        out.append(lambda x: x[0])

    for a in range(Q + 1):
        for b in range(Q + 1):
            out.append(
                lambda x, a=a, b=b: (
                    (np.cos(a * x[:, 0])) * (x[:, 1] / l_theta_dot) ** b
                )
            )
            if vectorized:
                out.append(
                    lambda x, a=a, b=b: (
                        (np.cos(a * x[:, 0])) * (x[:, 1] / l_theta_dot) ** b
                    )
                )
            else:
                out.append(
                    lambda x, a=a, b=b: ((np.cos(a * x[0])) * (x[1] / l_theta_dot) ** b)
                )

            if a != 0:
                if vectorized:
                    out.append(
                        lambda x, a=a, b=b: (
                            (np.sin(a * x[:, 0])) * (x[:, 1] / l_theta_dot) ** b
                        )
                    )
                else:
                    out.append(
                        lambda x, a=a, b=b: (
                            (np.sin(a * x[0])) * (x[1] / l_theta_dot) ** b
                        )
                    )

    return out
