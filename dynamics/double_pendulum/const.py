import math

m1 = 1.0  # in kg
m2 = 1.0  # in kg
l1 = 1.0  # in m
l2 = 1.0  # in m
g = 9.81  # in m/s²

l_theta1 = math.pi / 3
l_theta2 = math.pi / 3
l_theta_dot1 = 0.5
l_theta_dot2 = 0.5
