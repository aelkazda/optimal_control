import math
import numpy as np
import sympy

from .const import l_theta1, l_theta2, l_theta_dot1, l_theta_dot2


def polynomial(Q=0, vectorized=False, use_sympy=False):
    assert np.issubdtype(type(Q), np.integer) and Q >= 0

    out = []

    for a in range(Q + 1):
        b_upper = Q - a + 1
        for b in range(b_upper):
            c_upper = Q - a - b + 1
            for c in range(c_upper):
                d_upper = Q - a - b - c + 1
                for d in range(d_upper):
                    if vectorized:
                        out.append(
                            lambda x, a=a, b=b, c=c, d=d: (
                                (x[:, 0] / l_theta1) ** a
                                * (x[:, 1] / l_theta2) ** b
                                * (x[:, 2] / l_theta_dot1) ** c
                                * (x[:, 3] / l_theta_dot2) ** d
                            )
                        )
                    else:
                        out.append(
                            lambda x, a=a, b=b, c=c, d=d: (
                                (x[0] / l_theta1) ** a
                                * (x[1] / l_theta2) ** b
                                * (x[2] / l_theta_dot1) ** c
                                * (x[3] / l_theta_dot2) ** d
                            )
                        )

    return out


def fourier_poly(Q=0, vectorized=False, use_sympy=False):
    assert np.issubdtype(type(Q), np.integer) and Q >= 0

    out = []
    for i in range(2):
        if vectorized:
            out.append(lambda x, i=i: x[:, i])
        else:
            out.append(lambda x, i=i: x[i])

    if vectorized:
        sin = np.sin
        cos = np.cos
    else:
        sin = math.sin
        cos = math.cos

    if use_sympy:
        sin = sympy.sin
        cos = sympy.cos

    for a in range(Q + 1):
        for b in range(Q + 1):
            for c in range(Q + 1):
                for d in range(Q + 1 - c):
                    for f1 in (sin, cos):
                        for f2 in (sin, cos):
                            if a != 0 or f1 != sin:
                                if b != 0 or f2 != sin:
                                    if vectorized:
                                        out.append(
                                            lambda x, f1=f1, f2=f2, a=a, b=b, c=c, d=d: (
                                                (f1(a * x[:, 0]))
                                                * (f2(b * x[:, 1]))
                                                * (x[:, 2] / l_theta_dot1) ** c
                                                * (x[:, 3] / l_theta_dot2) ** d
                                            )
                                        )
                                    else:
                                        out.append(
                                            lambda x, f1=f1, f2=f2, a=a, b=b, c=c, d=d: (
                                                (f1(a * x[0]))
                                                * (f2(b * x[1]))
                                                * (x[2] / l_theta_dot1) ** c
                                                * (x[3] / l_theta_dot2) ** d
                                            )
                                        )

    return out
