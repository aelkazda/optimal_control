import math
import numpy as np
from .const import m1, m2, l1, l2, g
from ...base import model


def f(x=None, u=None):
    """
    x: State of the double pendulum
    [angle_1, angle_2, angular_velocity_1, angular_velocity_2]
    u: Control (None)

    Returns:
        Derivative of the state with respect to time
    """
    c, s = math.cos(x[0] - x[1]), math.sin(x[0] - x[1])
    s1 = math.sin(x[0])
    s2 = math.sin(x[1])

    th1_dot_sqr = x[2] * x[2]
    th2_dot_sqr = x[3] * x[3]

    accel_1 = (
        m2 * g * s2 * c
        - m2 * s * (l1 * th1_dot_sqr * c + l2 * th2_dot_sqr)
        - (m1 + m2) * g * s1
    ) / (l1 * (m1 + m2 * s * s))

    accel_2 = (
        (m1 + m2) * (l1 * th1_dot_sqr * s - g * s2 + g * s1 * c)
        + m2 * l2 * th2_dot_sqr * s * c
    ) / (l2 * (m1 + m2 * s * s))
    return np.array([x[2], x[3], accel_1, accel_2])


func = model.SystemFunc(f)
system = model.ContinuousSystem(4, func, 0.0, 20.0)
