from collections import namedtuple

import numpy as np
import scipy.integrate
import scipy.sparse

BatchDerivatives = namedtuple("BatchDerivatives", "F_j F_h L_j L_h Lf_j Lf_h")
Derivatives = namedtuple("Derivatives", "fx fu fxx fuu fux lx lu lxx luu lux")


class SystemFunc:
    """
    Evolution function for a continuous time dynamical system.
    """

    def __init__(
        self, func, jac_x=None, jac_u=None, hess_xx=None, hess_uu=None, hess_ux=None
    ):
        self.func = func
        self.x = jac_x
        self.u = jac_u
        self.xx = hess_xx
        self.uu = hess_uu
        self.ux = hess_ux

    def __call__(self, state, control=None):
        return self.func(state, control)


class ContinuousSystem:
    """
    Continuous time Dynamical system
    """

    def __init__(self, state_dim, system_func, t_i, t_f):
        self.state_dim = state_dim
        self.system_func = system_func
        self.t_i = t_i
        self.t_f = t_f

    def solve(self, initial_state, t_span, dt, control=None):
        if control is None:

            def func(_, y):
                return self.system_func.func(y)

        else:

            def func(t, y):
                return self.system_func.func(y, control(t))

        solution = scipy.integrate.solve_ivp(
            func,
            t_span,
            initial_state,
            t_eval=np.arange(*t_span, dt),
            atol=1e-8,
            rtol=1e-8,
        )
        return solution


class DiscreteSystem:
    """
    Discrete time Dynamical System
    """

    def __init__(self, state_dim, system_func, n_steps):
        self.state_dim = state_dim
        self.system_func = system_func
        self.n_steps = n_steps

    def solve(self, initial_state):
        out = np.empty((self.n_steps + 1, self.state_dim))
        out[0] = initial_state
        for i in range(0, self.n_steps):
            out[i + 1] = self.system_func(out[i])
        return out


class Bounds:
    def __init__(self, lower, upper):
        assert len(lower) == len(upper) and (np.array(lower) <= np.array(upper)).all()
        n = len(lower)
        self.lower = np.array(lower)
        self.upper = np.array(upper)
        self.I = scipy.sparse.identity(n, format="csc")


class Trajectory:
    def __init__(
        self, system, control=None, initial_state=None, states=None, data=None
    ):
        n = system.n_steps
        n_u = system.control_dim
        n_x = system.state_dim

        self.system = system
        self.state_dim = n_x
        self.control_dim = n_u

        self.active_bounds = np.zeros((n, n_u), dtype=np.bool)
        if not data is None:
            self.data = data
            return
        self.data = np.empty((n + 1, n_u + n_x))

        if control is None:
            return
        self.data[:n, n_x:] = control

        if states is None:
            system.solve(initial_state, self)
        else:
            self.data[:, :n_x] = states

    def update(self, initial_state):
        self.system.solve(initial_state, self.data)

    def states(self):
        return self.data[:, : self.state_dim]

    def control(self):
        return self.data[:-1, self.state_dim :]


class ControlSystem:
    """
    Discrete time Controlled Dynamical System
    Required methods are:
     - eval
     - solve
     - solve_one
     - cost_seq
     - f_derivs
     - l_derivs

     - from_ddp_output, if control is bounded
     - from_ddp_output_unbounded, otherwise
    """

    def __init__(self, state_dim, control_dim, bounds, n_steps, methods):
        self.state_dim = state_dim
        self.control_dim = control_dim
        self.bounds = bounds
        self.n_steps = n_steps
        self.methods = methods

    def solve(self, initial_state, traj):
        self.methods.solve(initial_state, traj.data)

    def batch_derivatives(self, traj):
        return BatchDerivatives(
            *self.methods.f_derivs(traj.data), *self.methods.l_derivs(traj.data)
        )

    def trajectory_from_feedback(
        self, traj_orig, feedforward, feedback, step, traj_new
    ):
        if self.bounds is None:
            return Trajectory(
                self,
                data=self.methods.from_ddp_output_unbounded(
                    traj_orig.data, feedforward, feedback, step, traj_new.data
                ),
            )
        return Trajectory(
            self,
            data=self.methods.from_ddp_output(
                traj_orig.data,
                self.bounds.lower,
                self.bounds.upper,
                feedforward,
                feedback,
                step,
                traj_new.data,
            ),
        )

    def cost_seq(self, traj):
        return self.methods.cost_seq(traj.data)
