from math import fsum

import numpy as np
import osqp
from scipy import sparse
from scipy.linalg import LinAlgError, cho_factor, cho_solve, norm, solve

from .model import Derivatives, SystemFunc, Trajectory


def cost(trajectory):
    return fsum(trajectory.system.cost_seq(trajectory).tolist())


def cost_diff(traj, other_traj):
    system = other_traj.system
    return fsum((system.cost_seq(traj) - system.cost_seq(other_traj)).tolist())


def cost_diff_cached(terms, other_traj):
    return fsum((terms - other_traj.system.cost_seq(other_traj)).tolist())


def get_derivatives(i, trajectory, batch_derivatives):
    nx = trajectory.system.state_dim
    derivs = Derivatives(
        fx=batch_derivatives.F_j[i, :, :nx],
        fu=batch_derivatives.F_j[i, :, nx:],
        fxx=batch_derivatives.F_h[i, :, :nx, :nx],
        fuu=batch_derivatives.F_h[i, :, nx:, nx:],
        fux=batch_derivatives.F_h[i, :, nx:, :nx],
        lx=batch_derivatives.L_j[i, :nx],
        lu=batch_derivatives.L_j[i, nx:],
        lxx=batch_derivatives.L_h[i, :nx, :nx],
        luu=batch_derivatives.L_h[i, nx:, nx:],
        lux=batch_derivatives.L_h[i, nx:, :nx],
    )
    return derivs


def Q_model(d, V):
    Q = SystemFunc(None)
    Q.x = d.lx + d.fx.T @ V.x
    Q.u = d.lu + d.fu.T @ V.x
    # V.x @ d.fxx.transpose(1, 0, 2) is equivalent to np.tensordot(V.x, d.fxx, axes=1)
    Q.xx = d.lxx + d.fx.T @ V.xx @ d.fx + V.x @ d.fxx.transpose(1, 0, 2)
    Q.uu = d.luu + d.fu.T @ V.xx @ d.fu + V.x @ d.fuu.transpose(1, 0, 2)
    Q.ux = d.lux + d.fu.T @ V.xx @ d.fx + V.x @ d.fux.transpose(1, 0, 2)
    return Q


def asymptotic_direction(system, trajectory, batch_derivatives, feedforward, feedback):
    du = np.empty((system.n_steps, system.control_dim))
    dx = np.zeros(system.state_dim)
    for i in range(system.n_steps):
        derivs = get_derivatives(i, trajectory, batch_derivatives)
        du[i] = feedforward[i] + feedback[i] @ dx
        dx = derivs.fx @ dx + derivs.fu @ du[i]
    return du


class ControlOptimizer:
    default_params = {
        "damping": 0.0,
        "damping_active": 0.1,
        "factor": 1.0,
        "min_damping": 1e-6,
        "min_factor": 2.0,
        "regularization": None,
    }

    def __init__(self, system, trajectory, params=None):
        self.system = system
        self.trajectory = trajectory
        self.params = ControlOptimizer.default_params.copy()
        self.params["regularization"] = np.identity(system.control_dim)
        if not params is None:
            for param in params.keys():
                self.params[param] = params[param]
        self.j_threshold = np.inf
        self.optimality = np.inf
        self.iterations = 0

    def display(self, result=(np.inf, None, None), verbosity=0):
        print(
            f"{self.iterations:4} | {cost(self.trajectory):22.15e}"
            + f" | {self.params['damping']:5.2e}"
            + f" | {self.params['damping_active']:5.2e}"
            + f" | {self.optimality:10.5e}"
        )

    def backtracking_line_search(self, grad, feedforward, feedback, quadratic_term):
        j_terms = self.system.cost_seq(self.trajectory)
        j = fsum(j_terms)
        step = 1.0
        i_min = -1
        cost_min = j
        new_traj = Trajectory(self.system)
        for i in range(30):
            self.system.trajectory_from_feedback(
                self.trajectory, feedforward, feedback, step, new_traj
            )
            if np.isnan(new_traj.states()[-1]).any():
                continue

            j_diff = cost_diff_cached(j_terms, new_traj)
            if j_diff > 0.1 * (
                grad @ (self.trajectory.control() - new_traj.control()).flatten()
                - quadratic_term * step ** 2
            ):
                return new_traj, step
            if j - j_diff < cost_min:  # j - j_diff = new_j
                i_min = i
                cost_min = j - j_diff
            step *= 0.5

        if i_min >= 0:
            self.system.trajectory_from_feedback(
                self.trajectory, feedforward, feedback, 0.5 ** i_min, new_traj
            )
            return (new_traj, 0.5 ** i_min)
        return self.trajectory, 0.0

    def optimum_line_search(self, feedforward, feedback):
        step = 1.0
        new_traj = Trajectory(self.system)
        for _ in range(30):
            self.system.trajectory_from_feedback(
                self.trajectory, feedforward, feedback, step, new_traj
            )
            if np.isnan(new_traj.states()[-1]).any():
                continue

            if cost(new_traj) < self.j_threshold:
                return new_traj, step
            step *= 0.5
        return self.trajectory, 0.0

    def increase_damping(self, inactive_only):
        self.params["factor"] = max(
            self.params["min_factor"], self.params["factor"] * self.params["min_factor"]
        )
        self.params["damping"] = max(
            self.params["min_damping"], self.params["damping"] * self.params["factor"]
        )
        if inactive_only:
            self.params["damping_active"] = (
                self.params["damping_active"] * self.params["min_factor"]
            )

    def decrease_damping(self):
        self.params["factor"] = min(
            1.0 / self.params["min_factor"],
            self.params["factor"] / self.params["min_factor"],
        )
        self.params["damping"] = self.params["damping"] * self.params["factor"]
        if self.params["damping"] < self.params["min_damping"]:
            self.params["damping"] = 0.0

    def reset_damping(self):
        self.params["damping"] = ControlOptimizer.default_params["damping"]
        self.params["damping_active"] = ControlOptimizer.default_params[
            "damping_active"
        ]
        self.params["factor"] = ControlOptimizer.default_params["factor"]

    def solve_model(self, quad_model, qp, iteration):
        u0 = self.trajectory.control()[iteration]
        n_u = quad_model.u.shape[0]
        n_x = quad_model.x.shape[0]
        reg_coeffs = (self.params["damping"], self.params["damping_active"])
        H = quad_model.uu + np.diag(
            reg_coeffs[0] * ~self.trajectory.active_bounds[iteration]
            + max(reg_coeffs) * self.trajectory.active_bounds[iteration]
        )
        M = norm(H)
        H /= M

        try:
            factor = cho_factor(H)
        except LinAlgError:
            self.increase_damping(False)
            return None, None, None, True
        if qp is None:
            result = -cho_solve(
                factor, np.hstack((quad_model.u[:, np.newaxis], quad_model.ux)) / M
            )
            k = result[:, 0]
            K = result[:, 1:]
        else:
            qp.update(Px=H[qp.triu_indices], q=quad_model.u / M - H @ u0)
            result = qp.solve()

            free = result.y == 0
            qp.active_bounds[iteration] = ~free

            for i in range(quad_model.u.size):
                if result.y[i] > 0:
                    result.x[i] = self.system.bounds.upper[i]
                if result.y[i] < 0:
                    result.x[i] = self.system.bounds.lower[i]

            k = result.x - u0
            K = np.zeros((n_u, n_x), order="f")
            try:
                K[free] = -solve(H[free].T[free], quad_model.ux[free] / M, sym_pos=True)
            except LinAlgError as e:
                raise e

        dv = min(0.5 * (k.T @ quad_model.uu @ k.T) + quad_model.u @ k, 0.0)
        return dv, k, K, False

    def line_search(self, grad):
        j_terms = self.system.cost_seq(self.trajectory)

        new_traj = Trajectory(self.system)

        default_step = 1.0
        step = default_step

        new_traj.data[: self.system.n_steps, self.system.state_dim :] = (
            self.trajectory.control() - grad * step
        )
        self.system.solve(self.trajectory.states()[0], new_traj.data)

        new_j_terms = self.system.cost_seq(new_traj)
        if (
            fsum((j_terms - new_j_terms).tolist()) < 0
            or np.isnan(new_traj.states()[-1]).any()
        ):
            while True:
                step *= 0.5
                new_traj.data[: self.system.n_steps, self.system.state_dim :] = (
                    self.trajectory.control() - grad * step
                )
                self.system.solve(self.trajectory.states()[0], new_traj.data)
                if np.isnan(new_traj.states()[-1]).any():
                    continue
                j_diff = cost_diff_cached(j_terms, new_traj)

                if j_diff > 0.1 * (
                    grad.flatten()
                    @ (self.trajectory.control() - new_traj.control()).flatten()
                ):
                    return new_traj, step

        new_traj_prev = Trajectory(self.system)
        while True:
            new_traj_prev.data = new_traj.data.copy()
            j_terms = new_j_terms

            step *= 2
            new_traj.data[: self.system.n_steps, self.system.state_dim :] = (
                self.trajectory.control() - grad * step
            )
            self.system.solve(self.trajectory.states()[0], new_traj.data)
            if np.isnan(new_traj.states()[-1]).any():
                return new_traj_prev, step * 0.5

            new_j_terms = self.system.cost_seq(new_traj)
            if fsum((j_terms - new_j_terms).tolist()) < 0:
                return new_traj_prev, step * 0.5

            new_traj_prev.data = new_traj.data
            j_terms = new_j_terms

    def one_grad_descent_iter(self):
        self.iterations += 1
        batch_derivatives = self.system.batch_derivatives(self.trajectory)
        grad = np.empty((self.system.n_steps, self.system.control_dim))
        p = batch_derivatives.Lf_j
        for i in range(self.system.n_steps - 1, -1, -1):
            derivs = get_derivatives(i, self.trajectory, batch_derivatives)
            grad[i] = derivs.lu + p @ derivs.fu
            p = p @ derivs.fx + derivs.lx
        self.trajectory, step = self.line_search(grad)
        return step

    def one_iteration(self):
        self.iterations += 1
        feedforward = [None] * (self.system.n_steps)
        feedback = [None] * (self.system.n_steps)

        batch_derivatives = self.system.batch_derivatives(self.trajectory)

        V_last = SystemFunc(None)
        V_last.x = batch_derivatives.Lf_j
        V_last.xx = batch_derivatives.Lf_h
        V = SystemFunc(None)

        # Backward pass
        grad = np.empty((self.system.n_steps, self.system.control_dim))

        qp = None
        if not self.system.bounds is None:
            qp = osqp.OSQP()
            qp.setup(
                P=sparse.csc_matrix(
                    np.ones((self.system.control_dim, self.system.control_dim))
                ),
                q=np.zeros(self.system.control_dim),
                A=self.system.bounds.I,
                l=self.system.bounds.lower,
                u=self.system.bounds.upper,
                verbose=False,
                polish=True,
                eps_abs=1e-12,
                eps_rel=1e-12,
            )
            qp.triu_indices = np.triu_indices(self.system.control_dim)
            qp.active_bounds = np.zeros(
                (self.system.n_steps, self.system.control_dim), dtype=np.bool
            )

        failure = True
        dV = [0.0] * self.system.n_steps
        while failure:
            V.x = V_last.x
            V.xx = V_last.xx
            p = V.x
            for i in range(self.system.n_steps - 1, -1, -1):
                derivs = get_derivatives(i, self.trajectory, batch_derivatives)
                grad[i] = derivs.lu + p @ derivs.fu
                p = p @ derivs.fx + derivs.lx
                quad_model = Q_model(derivs, V)

                dv, feedforward[i], feedback[i], failure = self.solve_model(
                    quad_model, qp, i
                )
                if failure:
                    break
                dV[i] = dv

                V.x = quad_model.x - feedback[i].T @ quad_model.uu @ feedforward[i]
                V.xx = quad_model.xx - feedback[i].T @ quad_model.uu @ feedback[i]
                V.xx += V.xx.T
                V.xx /= 2
            if failure:
                continue

            # Forward pass, asymptotic search direction
            du = asymptotic_direction(
                self.system, self.trajectory, batch_derivatives, feedforward, feedback
            )

            # Flip search direction if it's not a descent direction
            if fsum(du.flatten() * grad.flatten()) > 0:
                for i in range(self.system.n_steps):
                    feedforward[i] *= -1

            dv_total = fsum(dV)
            j = cost(self.trajectory)
            # TODO Absolute tolerance for dv_total
            if dv_total > 0 or abs(dv_total) / j < 1e-13:
                self.j_threshold = j + abs(j) * 1e-13

            if self.j_threshold == np.inf:
                self.trajectory, step = self.backtracking_line_search(
                    grad.flatten(),
                    feedforward,
                    feedback,
                    dv_total - du.flatten() @ grad.flatten(),
                )
            else:
                self.trajectory, step = self.optimum_line_search(feedforward, feedback)

            if step == 1.0 and not qp is None:
                self.trajectory.active_bounds = qp.active_bounds
            if step >= 0.5:
                self.decrease_damping()
            if step <= 1e-2:
                self.increase_damping(True)
            if step == 0:
                failure = True
                continue

            self.optimality = (
                norm(norm(du, axis=1), ord=np.inf)
                if self.params["damping"] == 0
                else np.inf
            )
            return grad, feedforward, feedback
