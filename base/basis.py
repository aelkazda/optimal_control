import numpy as np


class Basis:
    """
    Function basis
    """

    def __init__(self, state_dim, basis_generator, *args, **kwargs):
        self.vectorized_functions = basis_generator(vectorized=True, *args, **kwargs)
        self.functions = basis_generator(*args, **kwargs)
        self.symbolic_functions = basis_generator(use_sympy=True, *args, **kwargs)
        self.state_dim = state_dim
        self.n_func = len(self.functions)

    def __call__(self, state):
        out = np.empty(self.n_func)
        for i, func in enumerate(self.functions):
            out[i] = func(state)
        return out

    def vectorized(self, trajectory):
        out = np.empty((len(trajectory), self.n_func))
        for i, func in enumerate(self.vectorized_functions):
            out[:, i] = func(trajectory)
        return out

    def get_symbols(self, state):
        out = []
        for func in self.symbolic_functions:
            out.append(func(state))
        return out


class ControlBasis:
    """
    Function basis for controlled systems
    """

    def __init__(self, state_dim, control_dim, basis_generator, *args, **kwargs):
        self.vectorized_functions = basis_generator(vectorized=True, *args, **kwargs)
        self.functions = basis_generator(vectorized=False, *args, **kwargs)
        self.state_dim = state_dim
        self.control_dim = control_dim
        self.n_dim = state_dim + control_dim
        self.n_func = len(self.functions)

    def __call__(self, state, control):
        out = np.empty(self.n_func)
        for i, func in enumerate(self.functions):
            out[i] = func(state, control)
        return out

    def vectorized(self, trajectory):
        out = np.empty((len(trajectory), self.n_func))
        for i, func in enumerate(self.vectorized_functions):
            out[:, i] = func(
                trajectory[:, : self.state_dim], trajectory[:, -(self.control_dim) :]
            )
        return out
