import numpy as np
from scipy.linalg import lstsq, norm, solve

from .model import ControlSystem, DiscreteSystem, SystemFunc


def estimated_model(truncated_koopman, basis, n_steps, control=False, cost_funcs=None):
    if control:
        return ControlSystem(basis.state_dim, basis.control_dim, None, n_steps, basis)

    def func(state, _):
        psi = basis(state)
        return psi @ truncated_koopman

    system_func = SystemFunc(func)
    return DiscreteSystem(truncated_koopman.shape[1], system_func, n_steps)


def glue_trajectories(trajectories, controls):
    if controls is None:
        control = None
    else:
        control = np.vstack(controls)
    traj_current = np.vstack([traj[:-1, :] for traj in trajectories])
    traj_next = np.vstack([traj[1:, :] for traj in trajectories])
    return traj_current, traj_next, control


def koopman_discrete(
    basis, trajectories, controls, condition, full_matrix=False, return_data=False
):
    n_traj = sum((len(traj) for traj in trajectories))
    current = np.zeros((n_traj, basis.n_out))
    if full_matrix:
        nxt = np.zeros((n_traj, basis.n_out))
    else:
        nxt = np.zeros((n_traj, basis.state_dim))

    data = glue_trajectories(trajectories, controls)
    current, nxt = generate_data_discrete(basis, *data, condition, full_matrix)

    if return_data:
        k, _, _, eig = lstsq(current, nxt, cond=0)
    else:
        k, _, _, eig = lstsq(current, nxt, cond=0, overwrite_a=True, overwrite_b=True)
    print(f"Condition number: {eig[0]/eig[-1]} ")
    if return_data:
        return k, current, nxt
    return k


def generate_data_discrete(
    basis,
    trajectory_current,
    trajectory_nxt,
    control,
    condition=None,
    full_matrix=False,
):
    if control is None:
        psi = np.empty((trajectory_current.shape[0], basis.n_out))
        basis.eval(trajectory_current, psi)
    else:
        psi = np.empty((trajectory_current.shape[0], basis.n_out))
        basis.eval(np.hstack((trajectory_current, control)), psi)

    T = len(trajectory_current)

    if condition is None:
        keep_rows = np.arange(T)
    else:
        keep_rows = []
        for t in range(T):
            if condition(trajectory_current[t]):
                keep_rows.append(t)
        if keep_rows == []:
            return None, None
        keep_rows = np.array(keep_rows)

    current = psi[keep_rows]
    if full_matrix:
        nxt = np.empty((trajectory_nxt.shape[0], basis.n_out))
        basis.eval(trajectory_nxt, nxt)
    else:
        nxt = trajectory_nxt[keep_rows] - trajectory_current[keep_rows]
    return current, nxt
