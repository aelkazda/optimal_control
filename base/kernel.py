import numpy as np
from scipy.linalg import LinAlgError, cho_factor, cho_solve, solve
from scipy.optimize import minimize


class UnitKernel:
    def __init__(self, n_var):
        self.n_var = n_var
        self.n_param = 0

    def get_params(self):
        return np.array([])

    def set_params(self, params):
        pass


class PeriodicKernel:
    def __init__(self, params=None):
        self.n_var = 1
        self.n_param = 1
        self.params = 0.0 if params is None else float(np.array(params))

    def get_params(self):
        return np.array([self.params])

    def set_params(self, params):
        self.params = float(params)

    def __call__(self, X, Y):
        return np.exp(np.exp(self.params) * (np.cos(X - Y.T) - 1))

    def dp(self, X, Y, order=2):
        if order not in [0, 1, 2]:
            raise ValueError("Only derivatives up to second order can be computed.")

        C = np.exp(self.params) * (np.cos(X - Y.T) - 1)

        K = np.exp(C)
        dK = None
        d2K = None

        if order >= 1:
            dK = (C * K)[:, :, None]
        if order == 2:
            d2K = (dK * (1 + C[:, :, None]))[:, :, :, None]

        return K, dK, d2K

    def dx(self, X, Y, order=2):
        if order not in [0, 1, 2]:
            raise ValueError("Only derivatives up to second order can be computed.")

        C = np.exp(self.params) * np.cos(X - Y.T)
        K = np.exp(C - np.exp(self.params))
        dK = None
        d2K = None

        if order >= 1:
            S = np.exp(self.params) * np.sin(X - Y.T)
            dK = (-S * K)[:, :, None]
            if order >= 2:
                d2K = ((-C + S * S) * K)[:, :, None, None]

        return K, dK, d2K


class RBFKernel:
    def __init__(self, n_var, params=None):
        self.n_var = n_var
        self.n_param = n_var
        self.params = np.zeros(n_var) if params is None else np.array(params)

        self._cache_dp = None
        self._cache_dx = None

    def get_params(self):
        return self.params

    def set_params(self, params):
        self.params = params

    def __call__(self, X, Y):
        X = X * np.exp(self.params)
        Y = Y * np.exp(self.params)

        return np.exp(
            X @ Y.T
            - np.sum(X * X, axis=1)[:, None] / 2
            - np.sum(Y * Y, axis=1)[None, :] / 2
        )

    def dp(self, X, Y, order=2):
        if order not in [0, 1, 2]:
            raise ValueError("Only derivatives up to second order can be computed.")
        X = X * np.exp(self.params)
        Y = Y * np.exp(self.params)

        if order == 0:
            return (
                np.exp(
                    X @ Y.T
                    - np.sum(X * X, axis=1)[:, None] / 2
                    - np.sum(Y * Y, axis=1)[None, :] / 2
                ),
                None,
                None,
            )

        pairwise_diff2 = X[:, None, :] - Y[None, :, :]
        pairwise_diff2 *= pairwise_diff2

        K = np.exp(-np.sum(pairwise_diff2, axis=2) / 2)
        dK = None
        d2K = None

        if order >= 1:
            dK = -K[:, :, None] * pairwise_diff2
        if order == 2:
            self._update_cache_dp(pairwise_diff2)

            d2K = np.einsum(
                "ijk, ijl -> ijkl",
                pairwise_diff2,
                pairwise_diff2,
                optimize=self._cache_dp,
            )
            for i in range(self.n_var):
                d2K[:, :, i, i] -= 2 * pairwise_diff2[:, :, i]
            d2K = K[:, :, None, None] * d2K
        return K, dK, d2K

    def dx(self, X, Y, order=2):
        if order not in [0, 1, 2]:
            raise ValueError("Only derivatives up to second order can be computed.")

        exp_param = np.exp(self.params)

        X = X * exp_param[None, :]
        Y = Y * exp_param[None, :]
        pairwise_diff = X[:, None, :] - Y[None, :, :]

        K = np.exp(-np.sum(pairwise_diff * pairwise_diff, axis=2) / 2)
        dK = None
        d2K = None

        if order >= 1:
            tmp = exp_param[None, None, :] * pairwise_diff
            dK = -K[:, :, None] * tmp
        if order == 2:
            self._update_cache_dx(tmp)
            d2K = np.einsum("ijk, ijl -> ijkl", tmp, tmp, optimize=self._cache_dx)
            for i in range(self.n_var):
                d2K[:, :, i, i] -= exp_param[i] * exp_param[i]
            d2K = K[:, :, None, None] * d2K

        return K, dK, d2K

    def _update_cache_dp(self, M):
        if self._cache_dp is None:
            self._cache_dp = np.einsum_path(
                "ijk, ijl -> ijkl", M, M, optimize="optimal"
            )[0]

    def _update_cache_dx(self, M):
        if self._cache_dx is None:
            self._cache_dx = np.einsum_path(
                "ijk, ijl -> ijkl", M, M, optimize="optimal"
            )[0]


class ProductKernel:
    def __init__(self, components):
        self.components = components
        self.n_var = sum(ker.n_var for ker in components)
        self.n_param = sum(ker.n_param for ker in components)

    def get_params(self):
        return np.concatenate([ker.get_params() for ker in self.components])

    def set_params(self, params):
        idx = 0
        for ker in self.components:
            ker.set_params(params[idx : idx + ker.n_param])
            idx += ker.n_param

    def __call__(self, X, Y):
        K = 1.0
        var_idx = 0
        for ker in self.components:
            if not isinstance(ker, UnitKernel):
                start = var_idx
                end = var_idx + ker.n_var

                K *= ker(X[:, start:end], Y[:, start:end])
            var_idx += ker.n_var
        return K

    def dx(self, X, Y, order=2):
        if order not in [0, 1, 2]:
            raise ValueError("Only derivatives up to second order can be computed.")
        n = X.shape[0]
        m = Y.shape[0]

        K = np.ones((n, m))
        dK = np.zeros((n, m, self.n_var)) if order >= 1 else None
        d2K = np.zeros((n, m, self.n_var, self.n_var)) if order == 2 else None

        var_idx = 0
        for ker in self.components:
            if not isinstance(ker, UnitKernel):
                start = var_idx
                end = var_idx + ker.n_var
                result = ker.dx(X[:, start:end], Y[:, start:end], order)

                if order == 2:
                    d2K[:, :, :start, :start] *= result[0][:, :, None, None]
                    d2K[:, :, start:end, start:end] = K[:, :, None, None] * result[2]

                    d2K[:, :, :start, start:end] = np.einsum(
                        "ijk, ijl -> ijkl", dK[:, :, :start], result[1]
                    )

                    d2K[:, :, start:end, :start] = d2K[
                        :, :, :start, start:end
                    ].transpose([0, 1, 3, 2])

                if order >= 1:
                    dK[:, :, :start] *= result[0][:, :, None]
                    dK[:, :, start:end] = K[:, :, None] * result[1]

                K *= result[0]
            var_idx += ker.n_var
        return K, dK, d2K

    def dp(self, X, Y, order=2):
        if order not in [0, 1, 2]:
            raise ValueError("Only derivatives up to second order can be computed.")
        n = X.shape[0]
        m = Y.shape[0]

        K = np.ones((n, m))
        dK = np.zeros((n, m, self.n_param)) if order >= 1 else None
        d2K = np.zeros((n, m, self.n_param, self.n_param)) if order == 2 else None

        var_idx = 0
        param_idx = 0
        for ker in self.components:
            if not isinstance(ker, UnitKernel):
                start = param_idx
                end = param_idx + ker.n_param
                result = ker.dp(
                    X[:, var_idx : var_idx + ker.n_var],
                    Y[:, var_idx : var_idx + ker.n_var],
                    order,
                )

                if order == 2:
                    d2K[:, :, :start, :start] *= result[0][:, :, None, None]
                    d2K[:, :, start:end, start:end] = K[:, :, None, None] * result[2]

                    d2K[:, :, :start, start:end] = np.einsum(
                        "ijk, ijl -> ijkl", dK[:, :, :start], result[1]
                    )

                    d2K[:, :, start:end, :start] = d2K[
                        :, :, :start, start:end
                    ].transpose([0, 1, 3, 2])

                if order >= 1:
                    dK[:, :, :start] *= result[0][:, :, None]
                    dK[:, :, start:end] = K[:, :, None] * result[1]

                K *= result[0]
            var_idx += ker.n_var
            param_idx += ker.n_param
        return K, dK, d2K


def fit(kernel, reg, X_train, Y_train):
    n, d = Y_train.shape
    reg = np.zeros(d) if reg is None else reg

    K = kernel(X_train, X_train)
    diag = K.diagonal().copy()

    coefs = np.empty((n, d))
    for i in range(d):
        np.fill_diagonal(K, diag + np.exp(reg[i]))
        coefs[:, i] = solve(K, Y_train[:, i], sym_pos=True)
    return coefs


def param_optimize(kernel, reg_init, X_train, Y_train, X_valid, Y_valid, **kwargs):
    n, d = Y_train.shape
    m, _ = Y_valid.shape
    p = kernel.n_param

    def objective(x):
        """Validation error, given kernel hyperparameters and log of regularization
        coefficients.

        :x: Hyperparameter vector of size p+d. Contains kernel hyperparameters, followed
        by log of regularization coefficients.
        :returns: Validation error, scalar.

        """
        kernel.set_params(x[:p])
        reg = x[kernel.n_param :]

        K, _, _ = kernel.dp(X_train, X_train, order=0)
        diag = K.diagonal().copy()

        coefs = np.empty((n, d))
        sum_sqr_error = np.empty(d)
        for i in range(d):
            np.fill_diagonal(K, diag + np.exp(reg[i]))
            try:
                coefs[:, i] = solve(K, Y_train[:, i], sym_pos=True)
            except LinAlgError:
                return np.inf
            error = kernel(X_valid, X_train) @ coefs[:, i] - Y_valid[:, i]
            sum_sqr_error[i] = 0.5 * (error * error).sum()
        return sum_sqr_error.sum()

    def grad(x):
        """Gradient of validation error with respect to the kernel hyperparameters and
        log of regularization coefficients.

        :x: Hyperparameter vector of size p+d. Contains kernel hyperparameters, followed
        by log of regularization coefficients.
        :returns: Gradient vector of size p+d

        """
        kernel.set_params(x[:p])
        reg = x[p:]

        K, dK, _ = kernel.dp(X_train, X_train, order=1)
        diag = K.diagonal().copy()

        coefs = np.zeros((n, d))
        dcoefs = np.zeros((n, d, p + d))
        dsum_sqr_error = np.zeros((d, p + d))
        for i in range(d):
            np.fill_diagonal(K, diag + np.exp(reg[i]))
            try:
                cho = cho_factor(K)
            except LinAlgError:
                print(0)
                return np.zeros(p + d)
            alpha = coefs[:, i]  # shape: n
            dalpha = dcoefs[:, i, :]  # shape: (n, p+d)

            alpha[:] = cho_solve(cho, Y_train[:, i])
            for j in range(p):
                dalpha[:, j] = -cho_solve(cho, dK[:, :, j] @ alpha)
            dalpha[:, p + i] = -cho_solve(cho, np.exp(reg[i]) * alpha)

            # shape: (m, n), (m, n, p)
            K_valid, dK_valid, _ = kernel.dp(X_valid, X_train, order=1)

            error = K_valid @ alpha - Y_valid[:, i]  # shape: m
            derror_param = (
                dK_valid.transpose([0, 2, 1]) @ alpha + K_valid @ dalpha[:, :p]
            )  # shape: (m, p)

            dsum_sqr_error[i, :p] = error @ derror_param
            dsum_sqr_error[i, p + i] = error @ K_valid @ dalpha[:, p + i]
        return dsum_sqr_error.sum(0)

    def hess(x):
        """Hessian of validation error with respect to the kernel hyperparameters
        and log of regularization coefficients.

        :x: Hyperparameter vector of size p+d. Contains kernel hyperparameters, followed
        by log of regularization coefficients.
        :returns: Hessian matrix of shape (p+d, p+d)

        """
        kernel.set_params(x[:p])
        reg = x[p:]

        K, dK, d2K = kernel.dp(X_train, X_train, order=2)
        diag = K.diagonal().copy()
        coefs = np.zeros((n, d))
        dcoefs = np.zeros((n, d, p + d))
        d2coefs = np.zeros((n, d, p + d, p + d))

        d2sum_sqr_error = np.zeros((d, p + d, p + d))
        for i in range(d):
            np.fill_diagonal(K, diag + np.exp(reg[i]))
            try:
                cho = cho_factor(K)
            except LinAlgError:
                print(1)
                return np.zeros((p + d, p + d))

            def M(v, cho=cho):
                return cho_solve(cho, v)

            alpha = coefs[:, i]  # shape: n
            dalpha = dcoefs[:, i, :]  # shape: (n, p+d)
            d2alpha = d2coefs[:, i, :, :]  # shape: (n, p+d, p+d)

            alpha[:] = M(Y_train[:, i])

            dalpha[:, p + i] = -M(np.exp(reg[i]) * alpha)
            d2alpha[:, p + i, p + i] = M(
                np.exp(reg[i]) * (-2 * dalpha[:, p + i] - alpha)
            )
            for j in range(p):
                dalpha[:, j] = -M(dK[:, :, j] @ alpha)
                d2alpha[:, j, j] = -M(
                    2 * dK[:, :, j] @ dalpha[:, j] + d2K[:, :, j, j] @ alpha
                )
                for k in range(j):
                    d2alpha[:, k, j] = -M(
                        dK[:, :, j] @ dalpha[:, k]
                        + dK[:, :, k] @ dalpha[:, j]
                        + d2K[:, :, k, j] @ alpha
                    )
                    d2alpha[:, j, k] = d2alpha[:, k, j]

                d2alpha[:, j, p + i] = -M(
                    dK[:, :, j] @ dalpha[:, p + i] + np.exp(reg[i]) * dalpha[:, j]
                )
                d2alpha[:, p + i, j] = d2alpha[:, j, p + i]

            # shape: (m, n), (m, n, p)
            K_valid, dK_valid, d2K_valid = kernel.dp(X_valid, X_train, order=2)

            error = K_valid @ alpha - Y_valid[:, i]  # shape: m
            derror = np.zeros((m, p + d))
            d2error = np.zeros((m, p + d, p + d))

            derror[:, :p] = (
                dK_valid.transpose([0, 2, 1]) @ alpha + K_valid @ dalpha[:, :p]
            )
            derror[:, p + i] = K_valid @ dalpha[:, p + i]
            d2error[:, :p, :p] = (
                d2K_valid.transpose([0, 2, 3, 1]) @ alpha
                + (K_valid @ d2alpha[:, :p, :p].transpose([1, 0, 2])).transpose(
                    [1, 0, 2]
                )
                + 2 * dK_valid.transpose([0, 2, 1]) @ dalpha[:, :p]
            )
            d2error[:, p + i, p + i] = K_valid @ d2alpha[:, p + i, p + i]
            d2error[:, :p, p + i] = (
                dK_valid.transpose([0, 2, 1]) @ dalpha[:, p + i]
                + K_valid @ d2alpha[:, :p, p + i]
            )
            d2error[:, p + i, :p] = d2error[:, :p, p + i]

            d2sum_sqr_error[i] = derror.T @ derror + error @ d2error.transpose(
                [2, 0, 1]
            )
        return d2sum_sqr_error.sum(0)

    if kwargs.get("return_functions", False):
        return objective, grad, hess
    x_list = []
    obj_list = []

    def callback(x, state=None):
        """Called after each iteration of scipy.optimize.minimize.

        :x: Vector of hyperparameters and log of regularization coefficients.

        """
        obj = objective(x)
        print(f"{obj:.4e}")
        x_list.append(x.copy())
        obj_list.append(obj)

    opt_result = minimize(
        fun=objective,
        x0=np.r_[kernel.get_params(), np.zeros(d) if reg_init is None else reg_init],
        jac=grad,
        method="BFGS",
        callback=callback,
        tol=0,
        options={"gtol": 0.0, "disp": True, "maxiter": 100},
    )
    x = x_list[np.argmin(obj_list)]
    return x[:p], x[p:], obj_list
