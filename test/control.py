import numpy as np
from optimal_control.base.control import BatchDerivatives, ControlOptimizer, cost
from optimal_control.base.model import Bounds, ControlSystem, SystemFunc, Trajectory

np.random.seed(0)
n = 100
n_state = 4
n_control = 2
x0 = np.zeros(n_state)

target = np.zeros(n_state)
target[0] = 1.0
A = np.random.randn(n_state, n_state, n_state)
A = A + np.transpose(A, [0, 2, 1])
a = np.random.randn(n_state, n_state)
b = np.random.randn(n_state, n_control)


def f(x, u):
    return 0.5 * np.tensordot(A, x, axes=1) @ x + a @ x + b @ u


def fx(x, _):
    return x @ A + a


def fu(*_):
    return b


def fxx(*_):
    return A


def fuu(*_):
    return np.zeros((n_state, n_control, n_control))


def fux(*_):
    return np.zeros((n_state, n_control, n_state))


F = SystemFunc(f, fx, fu, fxx, fuu, fux)


def lf(x, *_):
    res = x - target
    return 10.0 * np.inner(res, res)


def lfx(x, *_):
    return 20.0 * (x - target)


def lfxx(*_):
    return 20.0 * np.eye(n_state)


def l(x, u):
    return np.inner(x, x) + 3.0 * np.inner(u, u)


def lx(x, _):
    return 2.0 * x


def lu(_, u):
    return 6.0 * u


def lxx(*_):
    return 2.0 * np.eye(n_state)


def luu(*_):
    return 6.0 * np.eye(n_control)


def lux(*_):
    return np.zeros((n_control, n_state))


L = []
for i in range(n):
    L.append(SystemFunc(l, lx, lu, lxx, luu, lux))
L.append(SystemFunc(lf, jac_x=lfx, hess_xx=lfxx))

bounds = Bounds(n_control * [-0.5], n_control * [2.0])
bounds = None
system = ControlSystem(n_state, n_control, bounds, F, L, n)

control = np.zeros((n, n_control))
traj = Trajectory(system, control, x0)

optimizer = ControlOptimizer(system, traj)
optimizer.params["damping"] = 0
for i in range(200):
    optimality = optimizer.one_iteration()[0]
    print(
        f"{i+1:4} | {cost(optimizer.trajectory):22.15e}"
        + f" | {optimizer.params['damping']:5.2e}"
        + f" | {optimizer.params['damping_active']:5.2e}"
        + f" | {optimality:10.5e}"
    )
