import subprocess
import sys

import numpy as np
from optimal_control.base.kernel import (
    PeriodicKernel,
    ProductKernel,
    RBFKernel,
    UnitKernel,
)

np.set_printoptions(linewidth=np.inf)
_, columns = map(int, subprocess.check_output(["stty", "size"]).split())


def test_kernel_derivs(kernel, n_var, n_param, which="both"):
    if which == "both":
        test_kernel_derivs(kernel, n_var, n_param, which="param")
        test_kernel_derivs(kernel, n_var, n_param, which="var")
    if which not in ["param", "var"]:
        return
    print("\n" + f" {kernel.__class__.__name__} ({which}) ".center(columns, "="))
    np.random.seed(0)
    X = np.random.randn(3, n_var)
    Y = np.random.randn(5, n_var)
    p = np.random.randn(n_param)

    kernel.set_params(p)
    if which == "param":
        dp_ = np.random.randn(n_param)
        for eps in [1e-3, 1e-4, 1e-5]:
            dp = eps * dp_

            K, dK, d2K = kernel.dp(X, Y, 2)
            kernel.set_params(p + dp)
            K_ = kernel(X, Y)

            print(f" eps = {eps:.1e} ".center(columns, "-"))
            print(K_ - K, end=2 * "\n")
            print(K_ - K - dK @ dp, end=2 * "\n")
            print(K_ - K - dK @ dp - 0.5 * dp @ d2K @ dp, end=2 * "\n")

    elif which == "var":
        dX_ = np.random.randn(*X.shape)
        for eps in [1e-3, 1e-4, 1e-5]:
            dX = eps * dX_

            K, dK, d2K = kernel.dx(X, Y, 2)
            K_ = kernel(X + dX, Y)

            print(f" eps = {eps:.1e} ".center(columns, "-"))
            d1 = K_ - K - np.einsum("ijk, ik -> ij", dK, dX)
            d2 = d1 - 0.5 * np.einsum(
                "ijl, il -> ij", np.einsum("ijkl, il -> ijk", d2K, dX), dX
            )
            print(K_ - K, end=2 * "\n")
            print(d1, end=2 * "\n")
            print(d2, end=2 * "\n")


option = sys.argv[1]
test_kernel_derivs(RBFKernel(n_var=3), 3, 3, which=option)
test_kernel_derivs(PeriodicKernel(), 1, 1, which=option)
test_kernel_derivs(
    ProductKernel([UnitKernel(1), RBFKernel(3), PeriodicKernel()]), 5, 4, which=option
)
