from multiprocessing import Pool
from time import time

import matplotlib.pyplot as plt
import numpy as np
import sympy
from optimal_control.base.basis import Basis
from optimal_control.base.model_estimation import estimated_model, koopman_discrete
from optimal_control.dynamics.double_pendulum.basis import fourier_poly, polynomial
from optimal_control.dynamics.double_pendulum.const import (
    l_theta1,
    l_theta2,
    l_theta_dot1,
    l_theta_dot2,
)
from optimal_control.dynamics.double_pendulum.model import system
from optimal_control.utils.plot import plot_continuous, plot_discrete

n_train = 5000
n_test = 50

t_i = 0.0
t_f = 5.0
dt = 0.01
n = 499
plot_n = 499

t_list = np.arange(t_i, t_f, dt)

train_trajectories = []
test_trajectories = []


L = np.array([l_theta1, l_theta2, l_theta_dot1, l_theta_dot2])
c = 1.2
L *= c


def generate_trajectory(x0=None):
    if x0 is None:
        while True:
            x0 = (2 * np.random.rand(4) - 1) * L

            solution = system.solve(x0, (t_i, t_f), dt)

            if solution.success:
                return solution.y.T
    else:
        return system.solve(x0, (t_i, t_f), dt).y.T


np.random.seed(0)
print(f"Generating {n_train} trajectories")
X0 = L * (2 * np.random.rand(n_train, 4) - 1)
clock0 = time()
with Pool(16) as p:
    train_trajectories = p.map(generate_trajectory, X0)
clock1 = time()
print(f"Done in {clock1 - clock0} seconds")

L /= c
for i in range(n_test):
    print(f"{i}/{n_test}", end="\r")
    test_trajectories.append(generate_trajectory())
print(f"{n_test}/{n_test}")

basis = Basis(4, polynomial, Q=4)
x = sympy.symbols("θ1 θ2 v1 v2")
basis_functions = basis.get_symbols(x)


def condition(state):
    return (np.abs(state) < L).all()


def energy(state):
    v = state[2:].copy()
    state = state[:2].copy()
    return (
        0.5 * v[1] ** 2
        + v[0] ** 2
        + v[0] * v[1] * np.cos(state[0] - state[1])
        - 2 * 9.81 * np.cos(state[0])
        - 9.81 * np.cos(state[1])
    )


E = []
for i in range(n_test):
    E.append(energy(test_trajectories[i][0]))

k, M, m = koopman_discrete(basis, train_trajectories, None, None, return_data=True)
koopman_model = estimated_model(k, basis, n)

params = (lambda x: [x[0], x[1], energy(x) - (min(E) + max(E)) / 2], 0, plot_n, dt)
var = (max(E) - min(E)) / 2
var = max(4, var + 0.5)
for i in range(n_test):
    sol = test_trajectories[i]
    koopman_sol = koopman_model.solve(sol[0])
    fig, ax = plot_discrete(koopman_sol, *params)
    fig, ax = plot_discrete(sol, *params, fig=fig, ax=ax)
    ax.set_ylim(-var, var)
    fig.savefig(f"plots/test/{i}.pdf")
    plt.close(fig)
