import numpy as np
from optimal_control.test import inverted_pendulum_kernel
from optimal_control.test.inverted_pendulum_kernel import (
    compute_error,
    gen_data,
    kernel,
    kernel_coefs,
    kernel_derivs,
)
from scipy.linalg import norm


def kernel_slow(X, Y, sigma=1.0, derivs=2):
    sqr1 = (X * X)[:, 2:].sum(axis=1) / (sigma * sigma)
    sqr2 = (Y * Y)[:, 2:].sum(axis=1) / (sigma * sigma)

    tmp = ((2 / sigma ** 2) * X[:, 2:]) @ Y[:, 2:].T - sqr1[:, None] - sqr2[None, :]

    K1 = np.exp(tmp)

    d = 8
    K2 = (1 + np.cos(X[:, 1][:, None] - Y[:, 1][None, :])) ** d

    if derivs >= 1:
        K1_d1 = K1 * tmp * (-2 / sigma)
        if derivs >= 2:
            K1_d2 = (4 / sigma ** 2) * K1 * (tmp * tmp + 1.5 * tmp)
            return (K1 * K2).T, (K1_d1 * K2).T, (K1_d2 * K2).T
        return (K1 * K2).T, (K1_d1 * K2).T
    return ((K1 * K2).T,)


def test_coeffs_derivs(reg, sigma, eps1, eps2):
    _coefs, g, H = kernel_coefs(X_train, y_train, reg, sigma)
    coefs_, _, _ = kernel_coefs(X_train, y_train, reg + eps1, sigma + eps2)

    d0 = coefs_ - _coefs
    d1 = d0 - g[0] * eps1 - g[1] * eps2
    d2 = d1 - H[0] * eps1 ** 2 / 2 - H[1] * eps2 ** 2 / 2 - H[2] * eps1 * eps2
    print(norm(d0), norm(d1), norm(d2), "", sep="\n")


def test_error_derivs(reg, sigma, eps1, eps2):
    test_error_derivs_(reg, sigma, eps1, 0.0)
    print()
    test_error_derivs_(reg, sigma, 0.0, eps2)
    print()
    test_error_derivs_(reg, sigma, eps1, eps2)


def test_error_derivs_(reg, sigma, eps1, eps2):
    test_error_, d1reg, d1sigma, d2reg, d2sigma, d1reg_d1sigma = compute_error(
        X_valid, y_valid, reg, sigma
    )
    test_error_2, _, _, _, _, _ = compute_error(
        X_valid, y_valid, reg + eps1, sigma + eps2
    )

    d0 = test_error_2 - test_error_
    d1 = d0 - d1reg * eps1 - d1sigma * eps2
    d2 = (
        d1
        - d2reg * eps1 ** 2 / 2
        - d2sigma * eps2 ** 2 / 2
        - d1reg_d1sigma * eps1 * eps2
    )
    print(d0, d1, d2, "", sep="\n")


def test_kernel_derivs(X, sigma, eps):
    K, K_d1, K_d2 = kernel(X, X, sigma=sigma)
    K_, _, _ = kernel(X, X, sigma=sigma + eps)
    d0 = K_ - K
    d1 = d0 - K_d1 * eps
    d2 = d1 - K_d2 * eps * eps / 2
    print(norm(d0), norm(d1), norm(d2), "", sep="\n")


if __name__ == "__main__":
    n_train = 800
    n_valid = n_train // 4
    n_test = n_train // 4
    params = np.array([4.9e-8, 8.261e1])

    X_train, y_train = gen_data(n_train)
    X_valid, y_valid = gen_data(n_valid)
    X_test, y_test = gen_data(n_test)
    inverted_pendulum_kernel.X_train = X_train
    inverted_pendulum_kernel.y_train = y_train
    inverted_pendulum_kernel.X_valid = X_valid
    inverted_pendulum_kernel.y_valid = y_valid
    inverted_pendulum_kernel.X_test = X_test
    inverted_pendulum_kernel.y_test = y_test

    test_error_derivs(1e-3, 2e0, 1e-2, 1e-3)
    test_error_derivs(1e-3, 2e0, 1e-3, 1e-4)

    i = 2
    j = 5

    d_x = np.random.randn(*(X_valid.shape))
    dx = d_x * 1e-5

    ker = kernel_derivs(X_train, X_valid, params[1], 2)
    ker_ = kernel_derivs(X_train, X_valid + dx, params[1], 0)

    d_0 = ker_ - ker[0]
    d_1 = d_0 - np.einsum("ijk, jk -> ij", ker[1], dx)
    d_2 = d_1 - 0.5 * np.einsum(
        "ijk, jk -> ij", np.einsum("ijkl, jl -> ijk", ker[2], dx), dx
    )

    print(ker_[i, j] - ker[0][i, j])
    print(ker_[i, j] - ker[0][i, j] - ker[1][i, j] @ dx[j])
    print(
        ker_[i, j]
        - ker[0][i, j]
        - ker[1][i, j] @ dx[j]
        - 0.5 * dx[j] @ ker[2][i, j] @ dx[j]
    )
