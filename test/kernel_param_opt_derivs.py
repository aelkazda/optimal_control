import numpy as np
from optimal_control.base.kernel import RBFKernel, param_optimize

np.set_printoptions(linewidth=np.inf, precision=3)

data = np.load("/sequoia/data1/aelkazda/tests/inv_pend/data/no_noise/train.npz")
n_train = 3
n_valid = 3
X_train = data["x"][:n_train]
Y_train = data["y"][:n_train]

X_valid = data["x"][-n_valid:]
Y_valid = data["y"][-n_valid:]

n_var = 5
out_dim = Y_train.shape[1]

np.random.seed(0)
kernel = RBFKernel(n_var=n_var, params=None)
f, g, h = param_optimize(
    kernel, None, X_train, Y_train, X_valid, Y_valid, return_functions=True
)

p = -2 * np.random.rand(kernel.n_param + out_dim)
dp_ = np.random.randn(*p.shape)

F = f(p)
G = g(p)
H = h(p)
for eps in [1e-1, 1e-2, 1e-3, 1e-4, 1e-5]:
    dp = dp_ * eps

    d0 = f(p + dp) - F
    d1 = d0 - G @ dp
    d2 = d1 - 0.5 * dp @ H @ dp
    print()
    print(d0)
    print(d1)
    print(d2)
