import subprocess

import numpy as np
from optimal_control.base.kernel import (
    PeriodicKernel,
    ProductKernel,
    RBFKernel,
    UnitKernel,
    fit,
    param_optimize,
)


def term_cols():
    _, columns = map(int, subprocess.check_output(["stty", "size"]).split())
    return columns


np.set_printoptions(linewidth=np.inf, precision=3)

data = np.load("/sequoia/data1/aelkazda/tests/inv_pend/data/no_noise/train.npz")
n_train = 500
n_valid = 100
n_test = 100

X_train = data["x"][:n_train]
Y_train = data["y"][:n_train]

X_valid = data["x"][n_train : n_train + n_valid]
Y_valid = data["y"][n_train : n_train + n_valid]

X_test = data["x"][-n_test:]
Y_test = data["y"][-n_test:]

kernel = ProductKernel(
    [UnitKernel(n_var=1), PeriodicKernel(params=None), RBFKernel(n_var=3, params=None)]
)

reg = None
param, reg, obj = param_optimize(kernel, reg, X_train, Y_train, X_valid, Y_valid)
kernel.set_params(param)

coefs = fit(kernel, reg, X_train, Y_train)
y_test_estimated = kernel(X_test, X_train) @ coefs
