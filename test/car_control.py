import matplotlib.pyplot as plt
import numpy as np
from optimal_control.base.control import ControlOptimizer
from optimal_control.base.model import Bounds, ControlSystem, Trajectory
from optimal_control.dynamics import car


def plot(trajectory):
    states = trajectory.states()
    fig, ax = plt.subplots()
    ax.plot(states[:, 0], states[:, 1])
    ax.plot(states[0, 0], states[0, 1], marker="o")
    ax.plot(states[-1, 0], states[-1, 1], marker="o")
    ax.plot([0], [0], marker="o")
    ax.set_ylim(-4, 4)
    ax.set_xlim(-4, 4)
    fig.savefig("plots/car.pdf")
    plt.close(fig)


n = 500

bounds = Bounds([-2.0, -0.5], [2.0, 0.5])
system = ControlSystem(4, 2, bounds, n, car)

ctrl_init = np.zeros(n, 2)
x_init = np.array([1.0, 1.0, 3 * np.pi / 2, 0.0])
traj = Trajectory(system, control=ctrl_init, states=x_init)

optimizer = ControlOptimizer(system, traj)
optimum_reached = False
i = 0
for i in range(60):
    optimizer.display()
    optimizer.one_iteration()
    if i % 5 == 0:
        plot(optimizer.trajectory)
optimizer.display()
