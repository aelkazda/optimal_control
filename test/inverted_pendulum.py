import math
from multiprocessing import Pool

import matplotlib.pyplot as plt
import numpy as np
from optimal_control.base.basis import ControlBasis
from optimal_control.base.control import ControlOptimizer
from optimal_control.base.model import Trajectory
from optimal_control.base.model_estimation import estimated_model, koopman_discrete
from optimal_control.dynamics import inv_pend as basis
from optimal_control.dynamics.inverted_pendulum.model import system
from optimal_control.utils.plot import plot_discrete

t = 5.0
plot_t = 5.0
dt = 0.01
n = int(round(t / dt))
try:
    k = np.load("k.npy")
except FileNotFoundError as e:
    n_train = 50000
    n_test = 10
    print(basis.n_out)

    train_trajectories = []
    train_controls = []
    test_trajectories = []
    test_controls = []

    def generate_trajectory(inp=None):
        if inp is None:
            while True:
                inp = M @ (2 * np.random.rand(5) - 1)
                x0 = inp[:4]
                control = inp[4:]

                solution = system.solve(x0, (0.0, dt + 1e-6), dt, lambda t: control)
                if solution.success:
                    return (solution.y.T.copy(), control[:, np.newaxis])
        else:
            solution = system.solve(inp[:4], (0.0, dt + 1e-6), dt, lambda t: inp[4:])
            return (solution.y.T.copy(), inp[4:][:, np.newaxis])

    np.random.seed(0)
    M = np.diag([5, np.pi, 5, 10, 5])
    X0 = (2 * np.random.rand(n_train, 5) - 1) @ M
    with Pool(12) as p:
        traj_ctrl = p.map(generate_trajectory, X0)
    train_trajectories = [traj for traj, ctrl in traj_ctrl]
    train_controls = [ctrl for traj, ctrl in traj_ctrl]

    dt = 0.01
    sample_dt = 0.1

    k, A, b = koopman_discrete(
        basis,
        train_trajectories,
        train_controls,
        None,
        full_matrix=False,
        return_data=True,
    )
    k = k.T.copy()
    np.save("k.npy", k)

basis.set_coeffs(k)
koopman_model = estimated_model(k, basis, n, control=True, cost_funcs=None)

# x0 = 0.1 * np.random.randn(4)
# x0[1] = np.pi
# u = 1.0 * np.sin(np.arange(0, t, dt)[:, np.newaxis])
traj = Trajectory(
    koopman_model,
    control=np.zeros((500, 1)),
    initial_state=np.array([0.0, np.pi, 0.0, 0.0]),
)

# control = lambda t: u[int(np.clip(t / dt, 0, n - 1))]
# sol = system.solve(x0, (0.0, t + 1e-6), dt, control).y.T

optimizer = ControlOptimizer(koopman_model, traj)
optimum_reached = False
i = 0
for i in range(60):
    optimizer.display()
    optimizer.one_iteration()
optimizer.display()

params = (lambda x: [x[0], x[1]], 0, n - 1, 0.0, dt)
ctrl_params = (lambda x: x, 0, n - 1, 0.0, dt)
fig, ax = plot_discrete(optimizer.trajectory.states(), *params)
# fig, ax = plot_discrete(sol, *params, fig, ax)
ax.set_ylim(-10, 20)
fig.savefig("plots/control/1.pdf")
plt.close(fig)
