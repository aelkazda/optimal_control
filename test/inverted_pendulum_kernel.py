from functools import lru_cache
from math import fsum
from multiprocessing import Pool

# pylint: disable=no-name-in-module, unused-import
import matplotlib.pyplot as plt
import numexpr as ne
import numpy as np
from optimal_control.base.control import ControlOptimizer, cost
from optimal_control.base.model import Bounds, ControlSystem, Trajectory
from optimal_control.base.model_estimation import glue_trajectories
from optimal_control.dynamics.inverted_pendulum.model import system
from optimal_control.utils.plot import plot_discrete
from scipy.linalg import cho_factor, cho_solve, norm, solve
from scipy.optimize import minimize

# Power of cosine kernel
d = 8

t = 5.0
plot_t = 5.0
dt = 0.01
sample_dt = 0.1
n = int(round(t / dt))
n_train = 500
n_valid = n_train // 4
n_test = n_train // 4


M = np.diag([5, np.pi, 10, 10, 15])
np.random.seed(0)


def generate_trajectory(inp=None):
    if inp is None:
        while True:
            inp = M @ (2 * np.random.rand(5) - 1)
            x0 = inp[:4]
            control = inp[4:]

            solution = system.solve(x0, (0.0, dt + 1e-6), dt, lambda t: control)
            if solution.success:
                return (solution.y.T.copy(), control[:, np.newaxis])
    else:
        solution = system.solve(inp[:4], (0.0, dt + 1e-6), dt, lambda t: inp[4:])
        return (solution.y.T.copy(), inp[4:][:, np.newaxis])


def gen_data(n):
    trajs_ = []
    ctrls_ = []

    X0 = (2 * np.random.rand(n, 5) - 1) @ M
    with Pool(12) as p:
        traj_ctrl = p.map(generate_trajectory, X0)
    trajs_ = [traj for traj, ctrl in traj_ctrl]
    ctrls_ = [ctrl for traj, ctrl in traj_ctrl]

    X_, y_, U_ = glue_trajectories(trajs_, ctrls_)

    return np.c_[X_, U_], y_ - X_


def prodT(a, b):
    return ne.evaluate("a * b", {"a": a, "b": b}).T


def kernel(X, Y, sigma=1.0, derivs=2):
    X_angle = X[:, 1]
    X = X[:, 2:]

    Y_angle = Y[:, 1]
    Y = Y[:, 2:]

    neg_pairwise_dist = ne.evaluate(
        "2 / s**2 * XY - XX - YY",
        {
            "s": sigma,
            "XY": X @ Y.T,
            "XX": ne.evaluate("sum(X * X / (sigma ** 2), axis=1)")[:, None],
            "YY": ne.evaluate("sum(Y * Y / (sigma ** 2), axis=1)")[None, :],
        },
    )

    K1 = ne.evaluate("exp(neg_pairwise_dist)")
    K2 = ne.evaluate(
        "(1 + cos(X - Y)) ** d", {"d": d, "X": X_angle[:, None], "Y": Y_angle[None, :]}
    )

    if derivs >= 1:
        K1_d1 = ne.evaluate(
            "-2 / s * K * D", {"s": sigma, "K": K1, "D": neg_pairwise_dist}
        )
        if derivs >= 2:
            K1_d2 = ne.evaluate(
                "4 / s**2 * K * D * (D + 1.5)",
                {"s": sigma, "K": K1, "D": neg_pairwise_dist},
            )
            return prodT(K1, K2), prodT(K1_d1, K2), prodT(K1_d2, K2)
        return prodT(K1, K2), prodT(K1_d1, K2)
    return (prodT(K1, K2),)


def kernel_derivs(X, Y, sigma, derivs=2):
    X_angle = X[:, 1]
    X = X[:, 2:]

    Y_angle = Y[:, 1]
    Y = Y[:, 2:]

    neg_pairwise_dist = ne.evaluate(
        "2/s**2 * XY - XX - YY",
        {
            "s": sigma,
            "XY": X @ Y.T,
            "XX": ne.evaluate("sum(X*X / (sigma ** 2), axis=1)")[:, None],
            "YY": ne.evaluate("sum(Y*Y / (sigma ** 2), axis=1)")[None, :],
        },
    )

    K1 = ne.evaluate("exp(D)", {"D": neg_pairwise_dist})
    K2 = ne.evaluate(
        "(1 + cos(X-Y))**d", {"d": d, "X": X_angle[:, None], "Y": Y_angle[None, :]}
    )
    K = prodT(K1, K2).T

    if derivs >= 1:
        grad = np.zeros((X.shape[0], Y.shape[0], 5))
        grad[:, :, 2:] = ne.evaluate(
            "2/s**2 * K * (X-Y)",
            {"s": sigma, "K": K[:, :, None], "X": X[:, None, :], "Y": Y[None, :, :]},
        )
        grad[:, :, 1] = ne.evaluate(
            "d * sin(X-Y) * (1+cos(X-Y))**(d-1) * K1",
            {"d": d, "X": X_angle[:, None], "Y": Y_angle[None, :], "K1": K1},
        )
        if derivs >= 2:
            hess = np.zeros((X.shape[0], Y.shape[0], 5, 5))

            diff = ne.evaluate("X-Y", {"X": X[:, None, :], "Y": Y[None, :, :]})
            square_diff = np.einsum("ijk,ijl->ijkl", diff, diff)
            tmp = ne.evaluate(
                "s**2/2 * I - DD",
                {"I": np.identity(3)[None, None, :, :], "DD": square_diff, "s": sigma},
            )

            hess[:, :, 2:, 2:] = ne.evaluate(
                "-4/s**4 * K * M", {"s": sigma, "K": K[:, :, None, None], "M": tmp}
            )

            # TODO: Optimize expression
            hess[:, :, 1, 1] = ne.evaluate(
                "d * (-C * (1+C)**(d-1) + (d-1) * (1 - C**2) * (1+C)**(d-2)) * K1",
                {
                    "d": d,
                    "K1": K1,
                    "C": ne.evaluate(
                        "cos(X-Y)", {"X": X_angle[:, None], "Y": Y_angle[None, :]}
                    ),
                },
            )

            hess[:, :, 1, 2:] = ne.evaluate(
                "G1 * G2 / K",
                {"G1": grad[:, :, 2:], "G2": grad[:, :, 1:2], "K": K[:, :, None]},
            )
            hess[:, :, 2:, 1] = hess[:, :, 1, 2:]
            return K, grad, hess
        return K, grad
    return K


def kernel_same(X, sigma=1.0, derivs=2):
    X_angle = X[:, 1]
    X = X[:, 2:]

    sqr = ne.evaluate("sum(X * X / (sigma ** 2), axis=1)")

    neg_pairwise_dist = ne.evaluate(
        "2 / s**2 * XX - sqr1 - sqr2",
        {"s": sigma, "XX": X @ X.T, "sqr1": sqr[:, None], "sqr2": sqr[None, :]},
    )

    K1 = ne.evaluate("exp(neg_pairwise_dist)")
    K2 = ne.evaluate(
        "(1 + cos(X1 - X2)) ** d",
        {"d": d, "X1": X_angle[:, None], "X2": X_angle[None, :]},
    )

    if derivs >= 1:
        K1_d1 = ne.evaluate(
            "-2 / s * K * D", {"s": sigma, "K": K1, "D": neg_pairwise_dist}
        )
        if derivs >= 2:
            K1_d2 = ne.evaluate(
                "4 / s**2 * K * D * (D + 1.5)",
                {"s": sigma, "K": K1, "D": neg_pairwise_dist},
            )
            return prodT(K1, K2), prodT(K1_d1, K2), prodT(K1_d2, K2)
        return prodT(K1, K2), prodT(K1_d1, K2)
    return (prodT(K1, K2),)


def kernel_coefs(X, y, reg, sigma, derivs=2):
    K, K_d1_sigma, K_d2_sigma = kernel_same(X, sigma=sigma)
    cho = cho_factor(K + reg * np.identity(n_train))

    K_coef = cho_solve(cho, y)
    if derivs >= 1:
        K_coef_d1_reg = -cho_solve(cho, K_coef)
        K_coef_d1_sigma = -cho_solve(cho, K_d1_sigma @ K_coef)

        if derivs >= 2:
            K_coef_d2_reg = -2 * cho_solve(cho, K_coef_d1_reg)
            K_coef_d2_sigma = -cho_solve(
                cho, 2 * (K_d1_sigma @ K_coef_d1_sigma) + K_d2_sigma @ K_coef
            )

            K_coef_d1_reg_d1_sigma = -cho_solve(cho, K_coef_d1_sigma) - cho_solve(
                cho, K_d1_sigma @ K_coef_d1_reg
            )
            return (
                K_coef,
                (K_coef_d1_reg, K_coef_d1_sigma),
                (K_coef_d2_reg, K_coef_d2_sigma, K_coef_d1_reg_d1_sigma),
            )
        return K_coef, (K_coef_d1_reg, K_coef_d1_sigma), None
    return K_coef, None, None


@lru_cache(maxsize=9)
def kernel_coefs_train(reg, sigma, derivs):
    return kernel_coefs(X_train, y_train, reg, sigma, derivs=derivs)


def compute_error(X, y, reg, sigma, derivs=2):
    coefs = kernel_coefs_train(reg, sigma, derivs=derivs)
    res = kernel(X_train, X, sigma, derivs=derivs)
    n_data = y.size

    test_error = 0.5 * norm(res[0] @ coefs[0] - y) ** 2 / n_data

    if derivs >= 1:
        test_error_d1_reg = (
            (res[0] @ coefs[0] - y) * (res[0] @ coefs[1][0])
        ).sum() / n_data
        test_error_d1_sigma = (
            (res[0] @ coefs[0] - y) * (res[0] @ coefs[1][1] + res[1] @ coefs[0])
        ).sum() / n_data

        if derivs >= 2:
            test_error_d2_reg = (
                ((res[0] @ coefs[1][0]) * (res[0] @ coefs[1][0])).sum()
                + ((res[0] @ coefs[0] - y) * (res[0] @ coefs[2][0])).sum()
            ) / n_data
            test_error_d2_sigma = (
                (
                    (res[1] @ coefs[0] + res[0] @ coefs[1][1])
                    * (res[0] @ coefs[1][1] + res[1] @ coefs[0])
                ).sum()
                + (
                    (res[0] @ coefs[0] - y)
                    * (
                        res[1] @ coefs[1][1]
                        + res[0] @ coefs[2][1]
                        + res[1] @ coefs[1][1]
                        + res[2] @ coefs[0]
                    )
                ).sum()
            ) / n_data

            test_error_d1_reg_d1_sigma = (
                (
                    (res[1] @ coefs[0] + res[0] @ coefs[1][1]) * (res[0] @ coefs[1][0])
                ).sum()
                + (
                    (res[0] @ coefs[0] - y)
                    * (res[1] @ coefs[1][0] + res[0] @ coefs[2][2])
                ).sum()
            ) / n_data
            return (
                test_error,
                test_error_d1_reg,
                test_error_d1_sigma,
                test_error_d2_reg,
                test_error_d2_sigma,
                test_error_d1_reg_d1_sigma,
            )
        return test_error, test_error_d1_reg, test_error_d1_sigma
    return test_error


def hyperparameter_objective(x):
    reg, sigma = np.exp(x)
    return compute_error(X_valid, y_valid, reg, sigma, derivs=0)


def hyperparameter_grad(x):
    reg, sigma = np.exp(x)
    return np.array(compute_error(X_valid, y_valid, reg, sigma, derivs=1)[1:]) * np.exp(
        x
    )


def hyperparameter_hess(x):
    reg, sigma = np.exp(x)
    derivs = compute_error(X_valid, y_valid, reg, sigma, derivs=2)[1:]
    A = np.diag(np.exp(x))
    return A @ np.array(((derivs[2], derivs[4]), (derivs[4], derivs[3]))) @ A + np.diag(
        np.exp(x) * derivs[:2]
    )


def callback(x):
    val = compute_error(X_test, y_test, *(np.exp(x)), derivs=0)
    params_list.append(np.exp(x))
    val_list.append(val)
    print(val, "| grad =", hyperparameter_grad(x))


class KernelEstimator:
    def __init__(self, coefs):
        self.debug = False
        self.coefs = coefs
        self.A = np.diag([1.0, 0.0, 1.0, 1.0, 0.01]) / (100 * n)
        self.B = np.diag([0.0, 1.0, 1.0, 1.0])

    def solve_one(self, data):
        return data[:4] + (
            kernel_derivs(X_train, data.reshape(1, 5), sigma=params[1], derivs=0).T
            @ self.coefs
        )

    def solve(self, x0, data):
        data[0, :4] = x0
        for i in range(n):
            data[i + 1, :4] = self.solve_one(data[i])

    def f_derivs(self, data):
        K, d1_K, d2_K = kernel_derivs(X_train, data, params[1], 2)
        grad = np.einsum("ij, ikl -> kjl", self.coefs, d1_K)
        hess = np.einsum("ij, iklm -> kjlm", self.coefs, d2_K)

        if self.debug:
            j = 20
            eps = 1e-2
            dx = np.random.randn(*(data.shape)) * eps
            K_ = kernel_derivs(X_train, data + dx, params[1], 0)
            K__ = kernel_derivs(X_train, data - dx, params[1], 0)
            print(((K_ + K__ - 2 * K).T @ self.coefs)[j])
            print(dx[j] @ hess[j][0] @ dx[j])

            s1 = []
            s2 = []
            for i in range(n_train):
                s1.append(((K_ + K__ - 2 * K)[i, j]) * self.coefs[i, 0])
                s2.append((dx[j] @ d2_K[i, j] @ dx[j]) * self.coefs[i, 0])

            print(fsum(s2) / fsum(s1))
            __import__("ipdb").set_trace()

        grad[:, :, :4] += np.identity(4)[None, :]
        return (grad, hess)

    def from_ddp_output(
        self, data_orig, lower, upper, feedforward, feedback, step, data_new
    ):
        data_new[0, :4] = data_orig[0, :4]
        for i in range(n):
            data_new[i, 4] = np.clip(
                data_orig[i, 4]
                + step * feedforward[i]
                + feedback[i] @ (data_new[i, :4] - data_orig[i, :4]),
                lower,
                upper,
            )
            data_new[i + 1, :4] = self.solve_one(data_new[i])

    def cost_seq(self, data):
        costs = np.empty(n + 1)
        costs[:n] = np.einsum("ij, ij -> i", data[:n] @ self.A, data[:n])
        costs[n] = data[n, :4] @ self.B @ data[n, :4]
        return costs

    def l_derivs(self, data):
        hess = np.repeat(self.A[None, :, :], n, axis=0)
        hess_f = self.B
        jac = data[:n] @ self.A
        jac_f = data[n, :4] @ self.B
        return jac, hess, jac_f, hess_f


if __name__ == "__main__":
    X_train, y_train = gen_data(n_train)
    X_valid, y_valid = gen_data(n_valid)
    X_test, y_test = gen_data(n_test)
    try:
        params = np.load(f"kernel_hyperparam_{n_train}.npy")
    except FileNotFoundError:
        params = np.array([-2.0, 1.0])
        val_list = [compute_error(X_test, y_test, *(np.exp(params)), derivs=0)]
        params_list = [np.exp(params)]

        try:
            opt_res = minimize(
                hyperparameter_objective,
                params,
                method="trust-exact",
                jac=hyperparameter_grad,
                hess=hyperparameter_hess,
                callback=callback,
                options={"disp": True, "gtol": 1e-11},
            )
        except np.linalg.LinAlgError:
            pass

        print(val_list[0] / min(val_list))
        params = params_list[np.argmin(val_list)]
        np.save(f"kernel_hyperparam_{n_train}.npy", params)

    alpha = solve(
        kernel_same(X_train, params[1], 0)[0] + params[0] * np.identity(n_train),
        y_train,
    )
    kernel_estimator = KernelEstimator(alpha)

    bounds = Bounds([-10.0], [10.0])
    ctrl_sys = ControlSystem(4, 1, bounds, n, kernel_estimator)

    x0 = np.array([0.0, np.pi, 0.0, 0.0])
    traj = Trajectory(ctrl_sys, control=np.zeros((n, 1)), initial_state=x0)

    optimizer = ControlOptimizer(ctrl_sys, traj)

    L = []
    try:
        for i in range(200):
            if i == 50:
                optimizer.j_threshold = min(L) + 2e-2
            optimizer.display()
            L.append(cost(optimizer.trajectory))
            g, ff, fb = optimizer.one_iteration()
    except KeyboardInterrupt:
        pass

    optimizer.display()

    u = optimizer.trajectory.control().copy()
    u_open = u.copy()
    states_orig = optimizer.trajectory.states()

    states = np.empty((n + 1, 4))
    states[0] = x0

    for i in range(n):
        u[i] = np.clip(u[i] + fb[i] @ (states[i] - states_orig[i]), -10.0, 10.0)
        sol = system.solve(
            states[i], (i * dt, (i + 1) * dt + 1e-6), dt, lambda t: u[i]
        ).y.T
        states[i + 1] = sol[1]
    control = lambda t: u_open[int(np.clip(t / dt, 0, n - 1))]
    sol = system.solve(x0, (0.0, t + 1e-6), dt, control).y.T

    plot_params = (lambda x: [x[1]], 0, n - 1, 0.0, dt)
    fig, ax = plot_discrete(optimizer.trajectory.states(), *plot_params)
    fig, ax = plot_discrete(states, *plot_params, fig, ax)
    fig, ax = plot_discrete(sol, *plot_params, fig, ax)
    fig.savefig(f"plots/kernel_trajectory.pdf")
    plt.close(fig)
